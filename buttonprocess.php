<?php
include_once __DIR__.'/include/dbdevice.php';
include_once __DIR__.'/include/functions.php';
include_once __DIR__.'/include/session.class.php';
include_once __DIR__.'/include/dbmember.php';
include_once __DIR__.'/include/devlibrary.php';

$session=new session();
$session->start_session("SMARTHOMESESSION",false);

/*
TODO Change when SSL hits
*/

if (check_login_status($mysql_member))
{
	if (isset($_POST['action']))
	{
		if (preg_match("/([0-9]+);(0|1)/", $_POST['action'],$matches)===1)
		{
			// if a good value is passed we parse it
			$returnval=askChange($matches[1],$matches[2],$_SESSION['member_id'],$mysql_device);
			echo json_encode($returnval);
		}
		else
		{
			echo json_encode(false);
		}
	}
	else
	{
		echo "Invalid Request";
	}

}
else
{
	header('Location: ./index.php');
	exit();
}

?>