<?php

/*
We can use this function to list device_id and stats
*/
function initiateDevs($dmysqlcon)
{
	if (!isset($dmysqlcon))
	{
		return false;
	}

	$result=array();

	if ($stmt=$dmysqlcon->prepare("SELECT dev_id,dev_status FROM devices"))
	{
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($did,$dstat);
		while ($stmt->fetch())
		{
			$result[$did]=$dstat;
		}
		$stmt->close();
		return $result;
	}
	else
	{
		return false;
	}

	
}

function askChange($dev_id,$dev_stat,$member_id,$dmysqlcon)
{
	if (!isset($dmysqlcon))
	{
		return false;
	}

	if ($stmt=$dmysqlcon->prepare("SELECT dev_status FROM devices WHERE dev_id=?"))
	{
		$stmt->bind_param("i",$dev_id);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($dstat);
		$stmt->fetch(); 

		if ($stmt->num_rows==1)
		{
			$stmt->close();
			if ($dstat!==$dev_stat)
			{
				if ($stmt2=$dmysqlcon->prepare("UPDATE devices SET dev_status=?,last_time_changed=now() WHERE dev_id=?"))
				{
					$stmt2->bind_param("ii",$dev_stat,$dev_id);
					$dmysqlcon->query("START TRANSACTION;");
					$stmt2->execute();
					$success=$dmysqlcon->query("COMMIT;");
					$counter=4;
					if ((!$success) && ($counter>=0))
					{
						$success=$dmysqlcon->query("COMMIT;");
						$counter--;
					}

					if (!$success)
					{
						// We have failed to commit
						return false;
					}
					else
					{
						// We can change so that if the log fails it doesn't try to change the device status
						$stmt2->close();
						$stmt3=$dmysqlcon->prepare("INSERT INTO device_log VALUES(?,?,?,now());");// dev_id,member_id,d_stat
						$stmt3->bind_param("iii",$dev_id,$member_id,$dev_stat);
						$stmt3->execute();
						$stmt3->close();
						return true;
					}
				}
				else
				{
					// Update query failed
					return false;
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			// No such device
			return false; 
		}
	}
	else
	{
		// Query failed
		return false;
	}
}

?>