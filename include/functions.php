<?php

include_once 'sconfig.php';
/*
-4  - Recomendation to enter captcha mode 
-3  - SQL query failed
-2  - No such user exists
-1  - User account blocked due to being bruteforced
 0  - Bad password
 1  - Login OK
*/
function login($email,$password,$mysqlcon1,$mysqlcon2=NULL)
{
	if (!isset($mysqlcon2))
	{
		$mysqlcon2=$mysqlcon1;
	}

	$statement=$mysqlcon1->prepare("SELECT member_id,username,password,salt,account_permissions,mobile_phone FROM members WHERE email=? LIMIT 1");

	if (!$statement)
		return -3;

	$statement->bind_param("s",$email);
	$statement->execute();
	$statement->store_result();
	$statement->bind_result($member_id,$username,$passworddb,$salt,$account_permissions,$mobile_phone);
	$statement->fetch();

	if ($statement->num_rows==1)
	{
		// we have a userhit - checking if he is being bruteforced
		$bruteres=checkbruteforce($member_id,$mysqlcon2);

		if ($bruteres==0)
		{
			$hashed_pass=hash("sha512",$password.$salt);
			$ip_addr=get_userIP();
			
			if ($hashed_pass==$passworddb)
			{
				$userbrowser=NULL;

				if (isset($_SERVER['HTTP_USER_AGENT'])) 
				{
					$userbrowser=$_SERVER['HTTP_USER_AGENT'];
				}
				else
				{
					$userbrowser='unknown';
				}

				$member_id=preg_replace("/[^0-9]+/", '', $member_id);
				$_SESSION['member_id']=$member_id;
				$_SESSION['username']=preg_replace("/[^0-9a-zA-Z_\-]+/", '', $username);
				$_SESSION['login_string']=hash("sha512",$ip_addr.$hashed_pass.$userbrowser);
				$_SESSION['last_activity']=time();

				if ($deletebad_statement=$mysqlcon2->prepare("DELETE FROM login_attempts WHERE id=?"))
				{
					$deletebad_statement->bind_param("i",$member_id);
					$deletebad_statement->execute();
				}

				return 1;
			}
			else
			{
				$member_id=preg_replace("/[^0-9]+/", '', $member_id);
				if ($mystatement = $mysqlcon2->prepare("INSERT INTO login_attempts VALUES(?,now(),?)"))
				{
					$mystatement->bind_param("is",$member_id,$ip_addr);
					$mystatement->execute();

					return 0;
				}
				else
					return -3;
			}
		}
		elseif ($bruteres==2)
		{
			return -1; // Bruteforced!!!
		}
		else
		{
			// 	TODO 
			//Enter Captcha mode 
			return -4; 
		}
	}
	else
		return -2; // No such user exists
}


/*
	Returns a user IP (idealy real)
*/
function get_userIP()
{
	$ip=NULL;

	if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	{
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	elseif (isset($_SERVER['REMOTE_ADDR']))
	{
		$ip=$_SERVER['REMOTE_ADDR'];
	}
	elseif (isset($_SERVER['SSH_CLIENT']))
	{
		$splithelp=explode(' ', $_SERVER['SSH_CLIENT']);
		$ip=$splithelp[0];
	}
	else
	{
		$ip="unknown";
	}

	return $ip;
}

/*
0 - Not being bruteforced
1 - To activate captcha mode
2 - To lock acc
*/
function checkbruteforce($member_id,$mysqlcon)
{
	$member_id=preg_replace("/[^0-9]+/", '', $member_id);
	$allowed_time=new DateTime();
	$allowed_time->sub(new DateInterval("PT3H")); // 3hrs in reverse is the watched time

	if ($statement_brute=$mysqlcon->prepare("SELECT * FROM login_attempts WHERE id=? AND time>=STR_TO_DATE(?,'%Y-%m-%d %H:%i:%s')"))
	{
		$statement_brute->bind_param("is",$member_id,$allowed_time->format("Y-m-d H:i:s"));
		$statement_brute->execute();
		$statement_brute->store_result();

		if ($statement_brute->num_rows<3)
			return 0; 
		elseif (($statement_brute->num_rows>2) && ($statement_brute->num_rows<5))
			return 1;
		else
			return 2;
		
	}
	else
		return 2;
}

function check_login_status($mysqlcon)
{
	if (isset($_SESSION['member_id'],$_SESSION['username'],$_SESSION['login_string'],$_SESSION['last_activity']))
	{
		if ((time()- $_SESSION['last_activity'])<IDLETIME)
		{
			$ip_addr=get_userIP();
			$userbrowser=NULL;
			if (isset($_SERVER['HTTP_USER_AGENT']))
			{
				$userbrowser=$_SERVER['HTTP_USER_AGENT'];
			}
			else
			{
				$userbrowser="unknown";
			}

			if ($member_statement=$mysqlcon->prepare("SELECT password FROM members WHERE member_id=? LIMIT 1"))
			{
				$member_id=preg_replace("/[^0-9]+/", '', $_SESSION['member_id']);
				$member_statement->bind_param("i",$member_id);
				$member_statement->execute();
				$member_statement->store_result();

				if ($member_statement->num_rows==1)
				{
					$member_statement->bind_result($dbpassword);
					$member_statement->fetch();

					$dbpassword=hash("sha512",$ip_addr.$dbpassword.$userbrowser);
					if ($_SESSION['login_string']==$dbpassword)
					{
						$_SESSION['last_activity']=time();
						return true;
					}
					else
					{
						// Possible session hijack
						// We can log this somewhere
						return false;
					}
				}
				else
					return false;
			}
			else
				return false;
		}
		else
		{
			$_SESSION=array();
			session_destroy();
			header('Location: ../logout.php');
		}
	}
	else
		return false;
}

?>