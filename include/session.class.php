<?php

class session
{
	
	function __construct()
	{
		session_set_save_handler(array($this,"open"), array($this,"close"), array($this,"read"), array($this,"write"), array($this,"destroy"), array($this,"gc"));
		register_shutdown_function("session_write_close");
	}

	function start_session($session_name,$secure)
	{
		$httponly=true;
		$session_lifetime=0;
		$entropy_length=512;
		$hash="sha512";

		if (in_array($hash, hash_algos()))
		{
			ini_set("session.hash_function",$hash);	
		}
		
		ini_set("session.hash_bits_per_character",6);

		if (ini_set("session.use_only_cookies",1)==FALSE)
		{
			header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
			exit();
		}

		$cookieParams=session_get_cookie_params();
		session_name($session_name);
		session_set_cookie_params($session_lifetime,$cookieParams["path"],$cookieParams["domain"],$secure,$httponly);
		ini_set("session.entropy_file","/dev/urandom");
		ini_set("session.entropy_length",$entropy_length);
		ini_set("session.use_trans_sid",0);
		session_start();
		// session_regenerate_id(true);
	}

	function open()
	{
		$host="localhost";
		$user="sessionuser"; 
		$pass="W{!^HIT#GM`<U(K])drT<Ren,A8Er4@e"; 
		$database_name="smart_home";
		if (!isset($this->db))
		{
			$mysqli=new mysqli($host,$user,$pass,$database_name);
			if ($mysqli->connect_error) 
			{
				header("Location: ../error.php?err=Error in session database");
				exit();
			}
			$this->db=$mysqli;
		}
		return true;
	}

	function close()
	{
		if (isset($this->read_statement))
		{
			$this->read_statement->close();
			// Close deallocates stmt handle so there is no need to call unset
		}
		if (isset($this->write_statement))
		{
			$this->write_statement->close();
		}

		if (isset($this->key_statement))
		{
			$this->key_statement->close();
		}
		
		$this->db->close();
		return true;
	}

	function getKey($session_id)
	{
		if (!isset($this->key_statement))
		{
			$this->key_statement=$this->db->prepare("SELECT session_key FROM sessions WHERE id = ? LIMIT 1");
		}

		$this->key_statement->bind_param("s",$session_id);
		$this->key_statement->execute();
		$this->key_statement->store_result();

		if ($this->key_statement->num_rows==1)
		{	
			// Key exists
			$this->key_statement->bind_result($key);
			$this->key_statement->fetch();

			return $key;
		}
		else
		{
			// We need to create a key
			$random_key=bin2hex(openssl_random_pseudo_bytes(64));
			return $random_key;
		}
		
	}


	/*
		encrypt is used to encrypt _SESSION variables, 
		PHP stores session variables in /tmp which, in case of shared hosting, is available to other people
	*/

	function encrypt($data,$session_key)
	{
		$session_key=hash("sha512",$session_key);
		$iv_size=openssl_cipher_iv_length('aes-256-cbc');
		$iv=openssl_random_pseudo_bytes($iv_size);
		$encrypted_data=openssl_encrypt($data, 'aes-256-cbc', $session_key,true,$iv);
		return base64_encode($iv.$encrypted_data); // Saljemo iv i encrypted data zajedno 
	}

	function decrypt($data,$key)
	{
		$key=hash("sha512",$key);
		$iv_size=openssl_cipher_iv_length('aes-256-cbc');
		$data=base64_decode($data);
		$iv=substr($data, 0,$iv_size);
		$hashed_data=substr($data, $iv_size);
		$decrypted_data=openssl_decrypt($hashed_data, 'aes-256-cbc', $key,true,$iv); 
		return $decrypted_data;
	}

	function read($session_id)
	{

		/* 
			We will use prepared statements throughout this class as :
				1. It's a good protection against SQL injection attacks.
				2. It creates less overhead for parsing the same queries multiple times(that differ in in one or two places)
		*/
		if (!isset($this->read_statement)) 
		{
			$this->read_statement=$this->db->prepare("SELECT data from sessions WHERE id=? LIMIT 1");
		}
		$this->read_statement->bind_param("s",$session_id); // Encrypted session id has type string
		$this->read_statement->execute();
		$this->read_statement->store_result();
		$this->read_statement->bind_result($data); 
		$this->read_statement->fetch();
		$key=$this->getKey($session_id);
		$data=$this->decrypt($data,$key);
		return $data;

	}

	function write($session_id,$data)
	{
		$session_key=$this->getKey($session_id);
		$data=$this->encrypt($data,$session_key);

		if (!isset($this->write_statement))
		{
			$this->write_statement=$this->db->prepare("REPLACE INTO sessions VALUES(?,now(),?,?)");
		}

		$this->write_statement->bind_param("sss",$session_id,$data,$session_key);
		$this->write_statement->execute();

		return true;
	}

	function destroy($session_id)
	{
		if (!isset($this->delete_statement))
		{
			$this->delete_statement=$this->db->prepare("DELETE FROM sessions WHERE id = ?");
		}

		$this->delete_statement->bind_param("s",$session_id);
		$this->delete_statement->execute();
		return true;
	}
	function gc($max)
	{
		if (!isset($this->gc_statement))
		{
			$this->gc_statement=$this->db->prepare("DELETE FROM sessions WHERE set_time < ?");
		}

		$datetime=new DateTime();
		$datetime->sub(new DateInterval('PT'.$max.'S'));
		$formated_dt=$datetime->format("Y-m-d H:i:s");
		$this->gc_statement->bind_param("s",$formated_dt);
		$this->gc_statement->execute();
		return true;
	}

}
?>