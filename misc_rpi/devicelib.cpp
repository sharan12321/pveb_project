#include "devicelib.hpp"
#include <utility>
#include <mysql_connection.h>
#include <cppconn/exception.h>
#include <cppconn/driver.h>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/statement.h>
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
#include <iostream>

void Devices::addDevice(SmartDevice *sd)
{
	device_map.insert(std::make_pair(sd->device_id,sd));
}

Devices::~Devices()
{
	
	for (Devices::iterator iter=this->begin();iter!=this->end();iter++)
	{
		SmartDevice *sd=iter->second;
		delete sd;
	}

}

void Devices::clearDeviceQueue()
{
	DeviceAction* da;
	while (!dev_queue.empty())
	{
		da=dev_queue.front();
		dev_queue.pop_front();
		delete da;
	}
}

SmartDevice* Devices::getDevice(uint16_t d_id)
{
	std::unordered_map<uint16_t,SmartDevice*>::const_iterator iter=device_map.find(d_id);

	if (iter==device_map.end())
		return nullptr;
	else
		return iter->second;

}


void Devices::queueHeadDevice(uint16_t did, uint16_t status, bool homechange)
{
	DeviceAction *dev_action=new DeviceAction(did,status,homechange);

	dev_queue.push_front(dev_action);
}

void Devices::queueDevice(uint16_t did, uint16_t status, bool homechange)
{
	DeviceAction *dev_action=new DeviceAction(did,status,homechange);

	dev_queue.push_back(dev_action);
}

DeviceAction* Devices::popDeviceQueue()
{
	DeviceAction* dev_action;
	
	if (!dev_queue.empty())
	{
		dev_action=dev_queue.front();
		dev_queue.pop_front();
	}
	else
		dev_action=nullptr;

	return dev_action;
}


// We received a change on the web need to inform the device
bool SmartDevice::webChange(RF24Network *network, uint16_t status)
{
	RF24NetworkHeader header(this->node_id); // 'T'=84 , returns ACK
	payload_t fromweb_payload;
	bool ok=false;
	short int counter=0;

	std::cout << "Sending to update device: " << this->device_id << "\t On nodeid: "<< this->node_id <<" with status: " << status << std::endl; //debugmode
	
	fromweb_payload.d_id=this->device_id;
	fromweb_payload.d_stat=status;
	ok = network->write(header,&fromweb_payload,sizeof(fromweb_payload)); 
	
	if ((!ok) && counter<3)
	{
		delay(50);
		ok= network->write(header,&fromweb_payload,sizeof(fromweb_payload));
		counter++;
	}

	if (ok)
		return true;
	else
		return false;
}


// We received a change in the house need to update database
bool SmartDevice::houseChange(sql::Connection *conn, uint16_t status)
{
	static sql::PreparedStatement *prep_stmt=nullptr;
	static sql::PreparedStatement *prep_stmt3=nullptr;
	static sql::Statement *stmt=nullptr;

	int update_count=0;
	int insert_count=0;

	std::cout << "Got a change in the house updating database..." << std::endl;
	if (prep_stmt==nullptr)
		prep_stmt=conn->prepareStatement("UPDATE devices SET dev_status=?,last_time_changed=now() WHERE dev_id=?");

	if (prep_stmt3==nullptr)
		prep_stmt3=conn->prepareStatement("INSERT INTO device_log VALUES(?,0,?,now())");

	if (stmt==nullptr)
		stmt=conn->createStatement();


	prep_stmt->setInt(1,status);
    prep_stmt->setInt(2,this->device_id);
    prep_stmt3->setInt(1,this->device_id);
	prep_stmt3->setInt(2,status);
	
    conn->setAutoCommit(false);
	//stmt->execute("START TRANSACTION");
	
	// Here we update         		
    update_count=prep_stmt->executeUpdate();
    insert_count=prep_stmt3->executeUpdate();

    conn->commit();
    //stmt->execute("COMMIT"); 
    conn->rollback();

    conn->setAutoCommit(true);

    if ((update_count+insert_count)==2)
    	return true;
    else
    	return false;
	
}