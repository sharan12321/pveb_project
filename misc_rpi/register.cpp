#include <string>
#include <openssl/sha.h>
#include <iostream>
#include <mysql_connection.h>
#include <cppconn/exception.h>
#include <cppconn/driver.h>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/statement.h>
#include <termios.h>
#include <unistd.h>
#include <boost/regex.hpp>
#include <openssl/rand.h>


// Compilation on RPI 2
// g++ -std=c++0x -lmysqlcppconn -lboost_regex -lcrypto -mtune=cortex-a7 -mfpu=neon-vfpv4 -mfloat-abi=hard register.cpp -o register

// Compilation regular CPU
// g++ -std=c++0x -lmysqlcppconn -lboost_regex -lcrypto register.cpp -o register

#define NUM_PSEUDO_BYTES_SALT 32

/*
	Prompts user to input a mobile phone and validates that it is in a correct form 
	and not taken. On error in SQL statements can throw SQLException

	Return value: 
		A available and correct mobile phone that user inputed
*/
std::string inputValidMobile(sql::Connection *conn)
{
	std::string mobile_phone;
	sql::PreparedStatement *prep_stmt;
	sql::ResultSet *res;
	bool valid=false;

	prep_stmt=conn->prepareStatement("SELECT mobile_phone FROM members WHERE mobile_phone=?");

	while (!valid)
	{
		std::cout << "Please enter your mobile phone number" << std::endl;
		std::cin >> mobile_phone;
		
		if (boost::regex_match(mobile_phone,boost::regex("(\\+|00)[1-9]{1}[0-9]{7,11}")))
		{
			prep_stmt->setString(1,mobile_phone);
			res=prep_stmt->executeQuery();
			if (res->rowsCount()==0)
			{
				valid=true;
			}
			else
			{
				std::cout << "Mobile phone already registered" << std::endl;
			}
			delete res;
		}
		else
		{
			std::cout << "Number format invalid" << std::endl;
		}
	}

	delete prep_stmt;
	return mobile_phone;
}

/*
	Prompts user to input an email and validates that it is in a correct form 
	and not taken. On error in SQL statements can throw SQLException

	Return value: 
		A available and correct email that user inputed
*/
std::string inputValidEmail(sql::Connection *conn)
{
	std::string email;
	sql::PreparedStatement *prep_stmt;
	sql::ResultSet *res;
	bool valid=false;

	prep_stmt=conn->prepareStatement("SELECT email FROM members WHERE email=?");
	
	while (!valid)
	{
		std::cout << "Please enter your email address" << std::endl;
		std::cin >> email;

		// Taken from http://www.regular-expressions.info/email.html
		if (boost::regex_match(email,boost::regex("[A-Za-z0-9._\%+-]+@(?:[A-Za-z0-9-]+\\.)+[A-Za-z]{2,}$")))
		{
			// Check if an email is already registered
			prep_stmt->setString(1,email);
			res=prep_stmt->executeQuery();
			if (res->rowsCount()==0)
			{
				valid=true;
			}
			else
			{
				std::cout << "Email already registered" << std::endl;
			}

			delete res;
		}
		else
		{
			std::cout << "Inputed email is not valid" << std::endl;
		}		
	}

	delete prep_stmt;

	return email;
}

/* 
	Prompts user to input a username and validates that it is in a correct form 
	and not taken. On error in SQL statements can throw SQLException

	Return value: 
		A available and correct username that user inputed
*/
std::string inputValidUsername(sql::Connection *conn)
{
	std::string username;
	sql::PreparedStatement *prep_stmt;
	sql::ResultSet *res;
	bool valid=false;

	prep_stmt=conn->prepareStatement("SELECT username FROM members WHERE username=?");
	
	while (!valid)
	{
		std::cout << "Please select your username, it must begin with a letter.\n\t(Valid characters are a-z, A-Z, _, -)" << std::endl;
		std::cin >> username;
		
		// We check that a GIVEN username is in the valid format as we will do with all user inputed variables
		if (boost::regex_match(username,boost::regex("[a-zA-Z][a-zA-Z_-]*")))
		{
			prep_stmt->setString(1,username);
			res=prep_stmt->executeQuery();

			if (res->rowsCount()==0)
			{
				valid=true;
			}
			else
			{
				std::cout << "Username taken - please try another" << std::endl;
			}
			delete res;
		}
		else
		{
			std::cout << "Username must begin with a letter" << std::endl;
		}
	}

	delete prep_stmt;

	return username;
}

/* 
	When we promt a user for a password we don't want his input to echo 
	(ECHO on stdin , we still want to output status on stdout)
	for that we need something similar to getch (from conio.h) that , sadly, gcc doesn't support
	This function is used to turn on/off echo on stdout 
*/
void initTermios(int echoMode)
{
	termios oldt;
	termios newt;
	tcgetattr(STDIN_FILENO,&oldt);
	newt=oldt;
	if (echoMode==0)
		newt.c_lflag &=~ECHO;
	else
		newt.c_lflag |=ECHO;
	tcsetattr(STDIN_FILENO,TCSANOW,&newt);
}

/* 
	Prompts user to input a password and validates that it is in a correct form. 
	On error in SQL statements can throw SQLException

	Return value: 
		A valid password that user inputed
*/
std::string inputPassword()
{
	std::string password;
	std::string rep_pass;

	bool valid=false;

	// We are entering a password input area - we do not want the user input to echo to stdout
	initTermios(0);
	passmissmatch:
	
	while (!valid)
	{
		std::cout << "Please select your password" << std::endl;
		std::cin >> password;
		valid=true;
		
		if (password.length()>15)
		{
			/* 
			   Password length is sufficient to say it it secure.
			   We can eventually not let too may characters matching (or growing order numeration for extra security) 
			*/
			break; 
		}

		if (password.length()<8)
		{
			valid=false;
			std::cout << "Password must be at least 8 characters long" << std::endl;
		}

		// Regex search looks for only a partial match as opposed to regex_match which matches the whole string
		if (!boost::regex_search(password.c_str(),boost::regex("[a-z]")))
		{
			valid=false;
			std::cout << "Password must contain at least one lowercase character, one upper case character and one special character OR be at least 16 characters long"<<std::endl;
		}

		if (!boost::regex_search(password.c_str(),boost::regex("[A-Z]")))
		{
			valid=false;
			std::cout << "Password must contain at least one lowercase character, one upper case character,one digit and one special character OR be at least 16 characters long"<<std::endl;
		}

		if (!boost::regex_search(password.c_str(),boost::regex("[^a-zA-Z0-9]")))
		{
			valid=false;
			std::cout << "Password must contain at least one lowercase character, one upper case character,one digit and one special character OR be at least 16 characters long"<<std::endl;
		}

		if (!boost::regex_search(password.c_str(),boost::regex("[0-9]")))
		{
			valid=false;
			std::cout << "Password must contain at least one lowercase character, one upper case character,one digit and one special character OR be at least 16 characters long"<<std::endl;	
		}
	}

	valid=false;

	// We will use goto if the confirmed password doesn't match
	std::cout << "Please confirm your password" << std::endl;
	std::cin >> rep_pass;
	
	if (rep_pass.compare(password)!=0)
	{
		std::cout << "Password missmatch, please enter your password again" << std::endl;
		goto passmissmatch;
	}


	initTermios(1);

	return password;

}

std::string sha512encrypt(std::string plaintext)
{
	unsigned char encrypted_binary[SHA512_DIGEST_LENGTH]; // 64bytes of encrypted data
	char hex_encrypted[SHA512_DIGEST_LENGTH*2+1]; // 1 byte = 2 hex characters + NULL byte at the end
	hex_encrypted[SHA512_DIGEST_LENGTH*2]='\0';

	//https://www.openssl.org/docs/manmaster/man3/SHA512.html
	SHA512((unsigned char *)plaintext.c_str(),plaintext.length(),(unsigned char *)&encrypted_binary);

	for (int i=0;i<SHA512_DIGEST_LENGTH;i++)
	{
		sprintf(&hex_encrypted[i*2],"%02x",(unsigned char)encrypted_binary[i]);
	}

	std::string str(hex_encrypted);
	return str; 

}

std::string generateSalt(int numBytes)
{
	int good_entropy;
	unsigned char salt_bytes[numBytes];
	char salt_hex[numBytes*2+1]; // 1 byte = 2 hexadecimal , and null char at the end

	salt_hex[numBytes*2]='\0';
	good_entropy=RAND_bytes((unsigned char*)salt_bytes,numBytes);

	while (good_entropy!=1)
	{
		std::cerr << "Entropy randomization failed... Trying again" << std::endl;
		sleep(5);
		good_entropy=RAND_bytes((unsigned char*)salt_bytes,numBytes);
	}
	
	std::cout << "Salt generation complete" << std::endl;
	for (int i=0;i<numBytes;i++)
		sprintf(&salt_hex[i*2],"%02x",salt_bytes[i]);

	return std::string(salt_hex);
}

int main()
{
	std::string username;
	std::string email;
	std::string password;
	std::string salt;
	std::string mobile_phone;
	sql::Driver *driver;
	sql::Connection *conn;
	sql::PreparedStatement *prep_stmt;
	//char ch; // user priviledges

	try 
	{

		// Examples of MySQL connector usage 
		// https://dev.mysql.com/doc/connector-cpp/en/

		sql::Driver *driver;
		sql::Connection *conn;

		driver=get_driver_instance();
		// q`'lu}_x!(Gvv{/4,~<'G8hxO&2/5q]0
		conn=driver->connect("tcp://127.0.0.1:3306","memberuser","q`\'lu}_x!\(Gvv\{/4,~<\'G8hxO&2/5q]0"); 
		conn->setSchema("smart_home"); // select db
		

		std::cout << "Welcome To Smart Home User Registration" << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;

		username=inputValidUsername(conn);
		email=inputValidEmail(conn);
		mobile_phone=inputValidMobile(conn);
		password=inputPassword();
		

		password=sha512encrypt(password);
		salt=generateSalt(NUM_PSEUDO_BYTES_SALT);

		// We need to store password hash in format sha512(pass + salt)
		password=sha512encrypt((password+salt));
		prep_stmt=conn->prepareStatement("INSERT INTO members(username,email,password,salt,account_permissions,mobile_phone) VALUES(?,?,?,?,'b',?)");
		prep_stmt->setString(1,username);
		prep_stmt->setString(2,email);
		prep_stmt->setString(3,password);
		prep_stmt->setString(4,salt);
		prep_stmt->setString(5,mobile_phone);
		prep_stmt->execute();
		std::cout << "User: \""<< username << "\" registered succesfully" << std::endl;

		

	}
	catch (sql::SQLException &e)
	{
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line: "<< __LINE__<<std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << ")" << std::endl;
	}
}