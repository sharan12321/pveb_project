#ifndef __DEVICELIB__HPP
#define __DEVICELIB__HPP

#include <unordered_map>
#include <deque>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>


class DeviceAction
{
	public:
	uint16_t dev_id;
	uint16_t dev_status;
	bool 	 home_change;

	inline DeviceAction()
	{}
	inline DeviceAction(uint16_t did,uint16_t stat,bool homech)
	:dev_id(did),dev_status(stat),home_change(homech)
	{}
};


// This should later be an abstract class that other ,device specific classes, will inherit
class SmartDevice
{
public:
	uint16_t device_id;
	uint16_t node_id;
	uint16_t status;
	bool to_init;

	/*
		This part is an optional upgrate to the system
		If one device is changed from the web and from home we want to give advantage to home user. (1 option)
		Other option is to give advantage to a specific user based on his/ her priviledges and put a lock on a device.

		We can say that if a device is changed in less than 5s interval increment no_changes. If no_changes==3 lock the device
	*/

	/*
		tm *last_changed; // ctime
		bool device_blocked; 
		unsigned short no_changes;
		int user_id_lock;
	*/

	SmartDevice(uint16_t did)
	:device_id(did),to_init(true)
	{}

	bool webChange(RF24Network *network, uint16_t status);
	bool houseChange(sql::Connection *conn, uint16_t status);
	
};




class Devices
{
private:
	std::unordered_map<uint16_t,SmartDevice*> device_map;
	std::deque<DeviceAction*> dev_queue; 
public:
	typedef std::unordered_map<uint16_t,SmartDevice*>::iterator iterator;
	typedef std::unordered_map<uint16_t,SmartDevice*>::const_iterator const_iterator;
	

	DeviceAction* popDeviceQueue();
	
	void clearDeviceQueue();
	
	inline void queueHeadDevice(DeviceAction *da)
	{
		dev_queue.push_front(da);
	}
	void queueHeadDevice(uint16_t did,uint16_t status,bool homechange);
	
	inline void queueDevice(DeviceAction *da)
	{
		dev_queue.push_back(da);
	}
	void queueDevice(uint16_t did,uint16_t status,bool homechange);

	void addDevice(SmartDevice *sd);
	inline void removeDevice(SmartDevice *sd)
	{
		device_map.erase(sd->device_id);
		// delete sd; 
	}
	SmartDevice* getDevice(uint16_t d_id);
	~Devices();
	inline iterator begin()
	{
		return device_map.begin();
	}
	inline const_iterator begin() const
	{
		return device_map.begin();
	}
	inline iterator end()
	{
		return device_map.end();
	}
	inline const_iterator end() const
	{
		return device_map.end();
	}
	
};



struct payload_t
{
	uint16_t d_id;
	uint16_t d_stat;
};

#endif