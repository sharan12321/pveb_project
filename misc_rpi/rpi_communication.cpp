#include <iostream>
#include <mysql_connection.h>
#include <cppconn/exception.h>
#include <cppconn/driver.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <string>
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
#include <cstdlib>
#include "devicelib.hpp"
#include <thread>         
#include <chrono> 

// Number of smart devices
#define DEVICE_NO 1

//octal addressing
const uint16_t this_node=00;

/*
	How long to wait before we continue checking again.
	Better, but allot harder solution would be to do these 2 things
	1. Create an SQL procedure that when table devices gets update  wakes up this thread 
	2. Set an interupt pin from NRF24L01 module so that we wake up this thread when we receive a new package

	We would need both of those to make it work with permanent sleep - but that would result in a low power consumption
*/
const unsigned long interval = 2000;

/*
	We are going to use SPI for communicating between our RPI 2 and the NRF24L01 attached to it

	Predefined constants:
	http://www.airspayce.com/mikem/bcm2835/group__constants.html#ga29bba154633d37d07fa130a86e4a1f2a

	Pinout:
	https://tmrh20.github.io/RF24/RPi.html

	CE pin is used for standy mode where CSN pin is used for SPI communication (Slave Select Pin in SPI (SS))
	https://sigrok.org/wiki/Protocol_decoder:Nrf24l01

	On RPI 2: CE - GPIO 15 (pin 22)					 CSN - SPI CS0 (pin) (pin 24 , GPIO 8)
			  MISO - GPIO 9 (pin 21)				 MOSI - GPIO 10 (pin 19)
			  SCKL - GPIO 11 (Pin 23)
*/
RF24 radio(RPI_V2_GPIO_P1_15,BCM2835_SPI_CS0,BCM2835_SPI_SPEED_8MHZ);
RF24Network network(radio);







void initiateDevices(Devices* dev)
{
	SmartDevice* sm=new SmartDevice(0); // Device id = 0
	sm->node_id=01;
	dev->addDevice(sm);
}

int main(int argc, char const *argv[])
{
	sql::Driver *driver;
	sql::Connection *conn;
	sql::PreparedStatement *prep_stmt;
	sql::PreparedStatement *prep_stmt2;
	sql::ResultSet *res;
	Devices devices;
	RF24NetworkHeader receive_header;
	payload_t home_payload;
	SmartDevice * sd;
	uint16_t did;
	uint16_t dstat;
	DeviceAction *dev_action;
	bool change_success=false;
	initiateDevices(&devices);

	try
	{
		driver=get_driver_instance();
		conn=driver->connect("tcp://127.0.0.1:3306","home_device_u","v!=>JB_B\(yF/-i\(P6i*8}SgH2D.\{FC0^");
		conn->setSchema("smart_home");
		prep_stmt=conn->prepareStatement("UPDATE devices SET dev_status=?,last_time_changed=now() WHERE dev_id=?");
		prep_stmt2=conn->prepareStatement("SELECT dev_id,dev_status FROM devices");

		radio.begin();
		delay(5);
		network.begin(/* channel */90,/*this node address*/this_node);
		radio.printDetails(); //debug mode

		while (1)
		{

			// We are re-routing packets and receiving them, all important actions are in this step
			network.update();

			while (network.available()) // Do we have packets for our node?
			{
				//sd=nullptr;
				std::cout << "We have a packet" << std::endl; //debug mode

				network.read(receive_header,&home_payload,sizeof(home_payload));

				std::cout << "Packet read: did: " << home_payload.d_id << " d_stat: " <<home_payload.d_stat << std::endl; // debug mode

				sd=devices.getDevice(home_payload.d_id);
				if (sd==nullptr)
				{
					std::cout << "Got a packet from an unknown device! Failure!" << std::endl;
					exit(EXIT_FAILURE);
				}

				if (sd->to_init)
	           	{
	           		// Program just started we need to initiate the values that our smart devices are sending and take
	           		// them as starting values
	           		sd->status=home_payload.d_stat;
	           		sd->to_init=false;
	           		prep_stmt->setInt(1,sd->status);
	           		prep_stmt->setInt(2,sd->device_id);
	           		prep_stmt->executeUpdate();
	           		std::cout << "Initiating done" << std::endl;

	           	}
	           	else
	           	{
	           		std::cout << "Post initiation processing" << std::endl; //debugmode

	           		if (sd->status!=home_payload.d_stat)
	           		{
	           			//sd->status=home_payload.d_stat;
	           			devices.queueDevice(sd->device_id,home_payload.d_stat,true);
	           			std::cout << "Marking house change" << std::endl; //debugmode
	           		}
	           	}
			}

			res=prep_stmt2->executeQuery();

			// Now we iterate through devices and their values in db to search for a change on the web
			while (res->next())
			{
				did=static_cast<uint16_t>(res->getInt("dev_id"));
				dstat=static_cast<uint16_t>(res->getInt("dev_status"));
				sd=devices.getDevice(did);
				//std::cout << "Got results dev_id:" <<did << " dev_status: " << dstat << std::endl;
				if (sd==nullptr)
				{
					//std::cout << "We have an unknown device registered on the web! Failure!" << std::endl;
					//exit(EXIT_FAILURE);

					// Since we are just showing the proof of concept we have devices that are not registered.
					continue; // TODO CHANGE ON FINAL VERSION
				}

				if ((!sd->to_init) && (sd->status!=dstat)) // If we have uninitialized devices wait for their packet
				{
						std::cout << "Got a change on the web" << std::endl; //debugmode
						//sd->status=dstat;
						devices.queueHeadDevice(sd->device_id,dstat,false);
				}
			}

			dev_action=devices.popDeviceQueue();

			while (dev_action!=nullptr)
			{
				/*
					Here we update devices and database values. Since we appended web changes to head. 
					If there is a same device with house and web change house change will be done last (and it will be the one staying)
				*/
				sd=devices.getDevice(dev_action->dev_id);
				sd->status=dev_action->dev_status;

				if (dev_action->home_change)
					change_success=sd->houseChange(conn,dev_action->dev_status);
				else
					change_success=sd->webChange(&network,dev_action->dev_status);


				if (change_success)
					std::cout << "Updating succeded" << std::endl;
				else
					std::cout << "Updating failed" << std::endl;
				delete dev_action;
				dev_action=devices.popDeviceQueue();
			}

			std::this_thread::sleep_for(std::chrono::milliseconds(interval)); //delay(interval);
		}
	
		
	}
	catch (sql::SQLException e)
	{
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ <<") on line: " << __LINE__ << std::endl;
		std::cout << "(MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << ")" << std::endl;
	}
	return 0;
}