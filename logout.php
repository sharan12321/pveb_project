<?php
include_once __DIR__.'/include/functions.php';
include_once __DIR__.'/include/session.class.php';

$session=new session();
$session->start_session("SMARTHOMESESSION",false); 
/*
TODO change once SSL is set up
*/

$_SESSION=array();

if (ini_get("session.use_cookies") || ini_get("session.use_only_cookies"))
{
 	$params=session_get_cookie_params();
 	setcookie(session_name(),'',time()-42000,$params['path'],$params['domain'],$params['secure'],$params['httponly']);
}

session_destroy();

header('Location: ../index.php');
exit;

?>