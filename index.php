<?php
include_once __DIR__.'/include/dbmember.php';
include_once __DIR__.'/include/dblogin_attempts.php';
include_once __DIR__.'/include/session.class.php';
include_once __DIR__.'/include/functions.php';


$session=new session();
$session->start_session("SMARTHOMESESSION",false); 
// TODO Change when SSL hits
?>
<!DOCTYPE html>
<html>
<head>
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
	<meta content="utf-8" http-equiv="encoding">
	<title>Smart Home Login: Credentials first please</title>
	<link rel="stylesheet" href="styles/loginform.css" type="text/css" />
	<script type="text/javascript" src="js/sha512.js"></script>
	<script type="text/javascript" src="js/forms.js"></script>
</head>
<body>
<?php
if (isset($_GET['error']))
{
	echo '<p class="error">Error Logging In!</p>';
	unset($_GET['error']);
}

?>

<div class="login-block">
 <form action="process_login.php" method="post" name="login_form" id="loginform">
 	Email: <input type="text" name="email" placeholder="Email" id="email" />
 	Password: <input type="password" name="password" id="password" placeholder="Password" />
 	<button onclick="formhash(this.form,this.form.password);">Log in</button>
 </form>
</div>

<?php  
if (check_login_status($mysql_member))
{
	//$user=preg_replace("/[^0-9a-zA-Z_\-]+/", '', $_SESSION['username']);
	//echo '<p>Currently logged in as '. htmlentities($user) . '</p>';
	//echo '<p>Do you want to change user? <a href="logout.php">Log out</a>.</p>';
	header('Location: ./homememberarea.php');
	exit();
}
?>

</body>
</html>