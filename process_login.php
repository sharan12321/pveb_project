<?php
include_once __DIR__.'/include/dbmember.php';
include_once __DIR__.'/include/dblogin_attempts.php';
include_once __DIR__.'/include/functions.php';
include_once __DIR__.'/include/session.class.php';

$session=new session();
$session->start_session("SMARTHOMESESSION",false); 
/*
TODO change when SSL is set up
*/

if (isset($_POST['email'],$_POST['pass']))
{
	$email=$_POST['email'];
	$password=$_POST['pass']; // sha512(plaintextpass)
	$loginres=login($email,$password,$mysql_member,$mysql_login_attempts);

	if ($loginres==1)
	{
		header('Location: ./homememberarea.php');
		exit();
	}
	else // TODO HERE we can add options for CAPTCHA mode / Account locked mode
	{
		$_POST['email']=NULL;
		$_POST['pass']=NULL;
		header('Location: ./index.php?error=1');
		exit();
	}
}
else
{
	echo "Invalid Request"; // Post variables not set
}

?>