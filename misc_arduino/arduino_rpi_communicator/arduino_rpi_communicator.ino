#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>

RF24 radio(7,8);       

RF24Network network(radio); 

const uint16_t this_node = 01;   
const uint16_t rpi_node = 00;  

const unsigned long interval = 2000;

unsigned long last_sent;

const int button_pin=6;
const int light_pin=5;

int button_state;
int last_button_state=LOW;
unsigned long last_debounce = 0;
unsigned long debounceDelay=50;

uint16_t light_status=LOW;
uint16_t light_device_id=00; // device id = 0 

struct payload_t {                  // Structure of our payload
  uint16_t d_id;
  uint16_t d_stat;
};

void setup(void)
{
  Serial.begin(57600);
  Serial.println("Setting up arduino-rpi communicator");
  
  pinMode(button_pin, INPUT);
  pinMode(light_pin,OUTPUT);
  digitalWrite(light_pin, light_status);
  
  SPI.begin();
  radio.begin();
  delay(5); // Give the module time to start
  radio.setPALevel(RF24_PA_MAX);
  network.begin(/*channel*/ 90, /*node address*/ this_node);
  
  network.update(); 
  /* 
    Here we try to initiate the device if RPI communicator is on
    if it is off it will need to wait for device change to get it initiated (enabled)
  */
  payload_t payload = { light_device_id, light_status };
  RF24NetworkHeader header(/*to node*/ rpi_node);
  bool ok = network.write(header,&payload,sizeof(payload));
  if (ok)
      Serial.println("Initialization of device successfull.");
  else
      Serial.println("Initialization of device failed.");
      
}

/*
  TODO 
  implement a sleep and interupt with multiple:
  pin from NRF module and pins from the devices
  
  That way we wake up whenever we receive a packet or change a device status
*/
void loop() 
{

  bool ok;
  unsigned long now=millis();
  int button_read= digitalRead(button_pin);
  network.update(); 
  
  while (network.available())
  {
      RF24NetworkHeader receive_header;
      payload_t payload;
      network.read(receive_header,&payload,sizeof(payload));
      Serial.println("Received a packet from master node:");
      Serial.print("Device id: ");
      Serial.print(payload.d_id);
      Serial.print(" With status: ");
      Serial.println(payload.d_stat);
      
     // Here we would ask for device status and search if that device_id's status is changed, but since we have one device there is no need for that in a POC
      if (payload.d_stat!=light_status)
      {
        light_status=payload.d_stat;
        digitalWrite(light_pin, light_status);
        Serial.println("Device status changed from web!");
      }
  }
  
  
  if (button_read!=last_button_state)
    last_debounce=millis();
    
  if ((millis() - last_debounce) > debounceDelay)
   if (button_read!=button_state)
   {
     button_state=button_read;
     
     if (button_state == HIGH) 
     {
       Serial.println("Device status changed");
       light_status = !light_status;
       
       digitalWrite(light_pin, light_status);
       payload_t payload = { light_device_id, light_status };
       RF24NetworkHeader header(/*to node*/ rpi_node);
       ok = network.write(header,&payload,sizeof(payload));
       if (ok)
          Serial.println("ok.");
       else
          Serial.println("failed.");
     }   
   }
   
   
   
   last_button_state=button_read;  
  
}
