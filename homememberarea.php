<?php  
include_once __DIR__.'/include/dbmember.php';
include_once __DIR__.'/include/functions.php';
include_once __DIR__.'/include/session.class.php';
include_once __DIR__.'/include/dbdevice.php';
include_once __DIR__.'/include/devlibrary.php';

$session=new session();
$session->start_session("SMARTHOMESESSION", false);
/*
TODO change when SSL is set up.
*/ 
?>

<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
	<title>Member Area</title>
	<link rel="stylesheet" href="styles/global.css" />
	<link rel="stylesheet" href="styles/switch.css" />
	<link rel="stylesheet" href="styles/room.css" />
	<link rel="stylesheet" href="styles/user.css" />
	<link rel="stylesheet" href="styles/device.css" />
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>
<body style="font-family: Calibri, sans-serif;">
<?php if (check_login_status($mysql_member)): ?>
<script type="text/javascript" src="js/display_stats.js"></script>
<script type="text/javascript">
function callPhp(inputobj) 
{
	var parsedid=inputobj.id.substr(13); // TODO change with regex OR PHP static array variable in HTML
	var ch=0;
    if (inputobj.checked) 
    {
        ch=1; 
    }

	var topass=''+parsedid+';'+ch; // We send 'deviceid;on_or_off'
	 $.ajax({
           type: "POST",
           url: 'http://192.168.1.12/buttonprocess.php',	// TODO CHANGE when SSL hits and we reserve a DNS name
           data:{'action':topass},
           dataType:'json', 
           success:function(data) 
           {
             var json=JSON.parse(data);
             if (json!==true)
	 		 {
	 		 	// We need to revert changes to button
	 		  	inputobj.checked=!(inputobj.checked);
	 		  	console.log("Failed to update device");
	 		 }
	 		 else
	 		 {
	 		 	console.log("Success");
	 		 }
	 	   }
	 	}); // Default is asynchronous ajax request
	 return false; // We are not following the link (to buttonprocess.php)
}
</script>

<div id="blur-background"> </div>

<header id="user-info">
<?php
	$user=preg_replace("/[^0-9a-zA-Z_\-]+/", '', $_SESSION['username']);
	echo '<p>Currently logged in as '. htmlentities($user) . ' | <a href="logout.php">Log out</a> </p> ';
	// echo '<p>Do you want to change user? <a href="logout.php">Log out</a>.</p>';
?>
</header>

<div id="wrapper">

	<div class="room-list-wrapper">
		<div class="room-list">
			<a href="homememberarea.php#LivingRoom"><div class="room-list-item"> Living room 
			</div> </a>
			<a href="homememberarea.php#Kitchen"><div class="room-list-item"> Kitchen
			</div></a>
			<a href="homememberarea.php#Bedroom"><div class="room-list-item"> Bedroom
			</div></a>

		</div>

		<div class="room-list">
			<a href="homememberarea.php#Bathroom"><div class="room-list-item"> Bathroom				
			</div></a>
			<a href="homememberarea.php#Garage"><div class="room-list-item"> Garage
			</div></a>
			<a href="homememberarea.php#Basement"><div class="room-list-item"> Basement
			</div></a>
		</div>
	</div>
	<!-- **************************************************************************************************** -->
	<?php 
		// TODO Listing tables of devices
		// $result = initiateDevs($mysql_device);
		// $rooms = array()
		// 
		// foreach($rooms as $location)
		// {
		//		echo "<div class='room'>";
		// 		echo "<header class='room-head'> <a name='{$location}'></a> {$location}";
		// 		echo "<table class='devices-table'>";
		//		
		//
		//		echo "</table>"
		// 		echo "</div>";
		// }
	?>

	<div class="room">
		<header class="room-head">
		<a name="LivingRoom"></a> Living room
		</header>
		<table class="devices-table">
			<tr>
				<th>Device name</th>
				
				<th>Power</th>
				<th>Status</th>
			</tr>
			<tr>
				<td>Light - main</td>
				
				<td>

					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch0" onclick="callPhp(this);" >
						<label class="onoffswitch-label" for="myonoffswitch0">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td>
					Connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
			<tr>
				<td>Light - lamp</td>
				
				<td>

					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch1" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch1">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td>
					Connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
			<tr>
				<td>Sunblind</td>
				
				<td>

					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch2" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch2">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td>
					Connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
			<tr>
				<td>TV</td>
				
				<td>

					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch3" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch3">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td>
					Connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
		</table>
	</div>
	<!-- **************************************************************************************************** -->
	<div class="room">
		<header class="room-head">
		<a name="Kitchen"></a> Kitchen
		</header>
		<table class="devices-table">
			<tr>
				<th>Device name</th>
				
				<th>Power</th>
				<th>Status</th>
			</tr>
			<tr>
				<td>Light - main</td>
				
				<td>

					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch4" onclick="callPhp(this);" >
						<label class="onoffswitch-label" for="myonoffswitch4">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td>
					Connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
			<tr>
				<td>Dishwasher</td>
				
				<td>

					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch5" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch5">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td>
					Connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
			<tr>
				<td>Fridge</td>
				
				<td>

					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch6" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch6">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td>
					Connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
			<tr>
				<td>Water heater</td>
				
				<td>

					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch7" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch7">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td>
					Connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
			
		</table>
	</div>
	<!-- **************************************************************************************************** -->
	<div class="room">
		<header class="room-head">
		<a name="Bedroom"></a>	Bedroom
		</header>
		<table class="devices-table">
			<tr>
				<th>Device name</th>
				
				<th>Power</th>
				<th>Status</th>
			</tr>
			<tr>
				<td>Light - main</td>				
				<td>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch8" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch8">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td class="connected" style="color: red">
					Not connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
			<tr>
				<td>Air condition</td>
				
				<td>

					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch9" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch9">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td class="connected" style="color: green">
					Connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
			<tr>
				<td> Sunblind </td>
				
				<td>

					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch10" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch10">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<span class=""></span>
				</td>
				<td>
					Connected
					<span class=""></span>
					<span class=""></span>
				</td>
			</tr>
		</table>
	</div>
	<!-- **************************************************************************************************** -->
	<div class="room">
		<header class="room-head">
		<a name="Bathroom"></a>	Bathroom
		</header>
		<table class="devices-table">
			<tr>
				<th>Device name</th>
				
				<th>Power</th>
				<th>Status</th>
			</tr>
			<tr>
				<td> Light - main</td>
				<td>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch11" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch11">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</td>
				<td> Connected </td>
			</tr>
			<tr>
				<td> Water heater </td>
				<td>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch12" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch12">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</td>
				<td style="color: red;"> Not connected </td>
			</tr>
			<tr>
				<td> Washing machine </td>
				<td>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch13" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch13">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</td>
				<td> Connected </td>
			</tr>
		</table>
	</div>
	<!-- **************************************************************************************************** -->	
	<div class="room">
		<header class="room-head">
		<a name="Garage"></a> Garage
		</header>
		<table class="devices-table">
			<tr>
				<th>Device name</th>
				
				<th>Power</th>
				<th>Status</th>
			</tr>
			<tr>
				<td> Garage doors </td>
				<td>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch14" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch14">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</td>
				<td> Connected </td>
			</tr>
			<tr>
				<td> Light - main </td>
				<td>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch15" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch15">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</td>
				<td> Connected </td>
			</tr>
		</table>
	</div>
	<!-- **************************************************************************************************** -->
	<div class="room">
		<header class="room-head">
		<a name="Basement"></a>	Basement
		</header>
		<table class="devices-table">
			<tr>
				<th>Device name</th>
				
				<th>Power</th>
				<th>Status</th>
			</tr>
			<tr>
				<td> Light - main </td>
				<td>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch16" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch16">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</td>
				<td> Connected </td>
			</tr>
			<tr>
				<td> Freezer </td>
				<td>
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch17" onclick="callPhp(this);" checked>
						<label class="onoffswitch-label" for="myonoffswitch17">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</td>
				<td> Connected </td>
			</tr>
		</table>
	</div>
	<!-- **************************************************************************************************** -->
</div>

<?php

	$result = initiateDevs($mysql_device);
	foreach ($result as $did => $status) 
	{
		echo '<script type="text/javascript">' . 'displayVals(' . $did . ', '. $status . ');' . '</script>';
	}
	//echo "<pre>";
	//print_r($result);
	//echo "</pre>";

?>

<?php else: ?>
<?php
header('Location: ./index.php?error=1');
exit();
?>
<?php endif; ?>
</body>
</html>